namespace PcelariStorefront.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Refactoredcart : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Carts", "ProductId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Carts", "ProductId", c => c.String());
        }
    }
}
