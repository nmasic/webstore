namespace PcelariStorefront.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderProductupdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderProducts", "MerchantId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderProducts", "MerchantId");
        }
    }
}
