namespace PcelariStorefront.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendedUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Name", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.AspNetUsers", "Surname", c => c.String(nullable: false, maxLength: 100));
            AddColumn("dbo.AspNetUsers", "OIB", c => c.String(nullable: false, maxLength: 11));
            AddColumn("dbo.AspNetUsers", "AdminChecked", c => c.Boolean());
            AddColumn("dbo.AspNetUsers", "FullAddress", c => c.String(nullable: false, maxLength: 400));
            AddColumn("dbo.AspNetUsers", "SecretKey", c => c.String(nullable: false, maxLength: 5));
            AddColumn("dbo.AspNetUsers", "IsBeeKeper", c => c.Boolean());
            AddColumn("dbo.AspNetUsers", "DateOfRegister", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "DateOfRegister");
            DropColumn("dbo.AspNetUsers", "IsBeeKeper");
            DropColumn("dbo.AspNetUsers", "SecretKey");
            DropColumn("dbo.AspNetUsers", "FullAddress");
            DropColumn("dbo.AspNetUsers", "AdminChecked");
            DropColumn("dbo.AspNetUsers", "OIB");
            DropColumn("dbo.AspNetUsers", "Surname");
            DropColumn("dbo.AspNetUsers", "Name");
        }
    }
}
