namespace PcelariStorefront.DAL.Migrations
{
    using Microsoft.AspNet.Identity;
    using Models.DomainModel;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PcelariStorefront.DAL.PcelariStoreDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PcelariStorefront.DAL.PcelariStoreDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            //PasswordHasher hasher = new PasswordHasher();
            //string passHash = hasher.HashPassword("admin");

            //Guid guid = Guid.NewGuid();

            context.Roles.AddOrUpdate(r => r.Id,
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("Admin"),
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("StandardUser"),
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole("BeeKeeper"));


            //context.Users.AddOrUpdate(u => u.Id, new User
            //{
            //    SecurityStamp = guid.ToString(), Email = "admin@admin.com", UserName = "admin", PasswordHash =  passHash, Name = "Nikola", Surname = "Ma�i�", OIB = "44594223190", AdminChecked = true,
            //    FullAddress = "Lani�te 20, Zagreb", SecretKey = "nmsb1", IsBeeKeper = false, DateOfRegister = DateTime.Now  
            //});



        }
    }
}
