namespace PcelariStorefront.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoredEntityBaseClass : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Carts", "CreatedByUser", c => c.String());
            AddColumn("dbo.Carts", "ModifiedByUser", c => c.String());
            AddColumn("dbo.OrderProducts", "CreatedByUser", c => c.String());
            AddColumn("dbo.OrderProducts", "ModifiedByUser", c => c.String());
            AddColumn("dbo.Orders", "CreatedByUser", c => c.String());
            AddColumn("dbo.Orders", "ModifiedByUser", c => c.String());
            AddColumn("dbo.Payments", "CreatedByUser", c => c.String());
            AddColumn("dbo.Payments", "ModifiedByUser", c => c.String());
            AddColumn("dbo.Products", "CreatedByUser", c => c.String());
            AddColumn("dbo.Products", "ModifiedByUser", c => c.String());
            AddColumn("dbo.ProductTypes", "CreatedByUser", c => c.String());
            AddColumn("dbo.ProductTypes", "ModifiedByUser", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductTypes", "ModifiedByUser");
            DropColumn("dbo.ProductTypes", "CreatedByUser");
            DropColumn("dbo.Products", "ModifiedByUser");
            DropColumn("dbo.Products", "CreatedByUser");
            DropColumn("dbo.Payments", "ModifiedByUser");
            DropColumn("dbo.Payments", "CreatedByUser");
            DropColumn("dbo.Orders", "ModifiedByUser");
            DropColumn("dbo.Orders", "CreatedByUser");
            DropColumn("dbo.OrderProducts", "ModifiedByUser");
            DropColumn("dbo.OrderProducts", "CreatedByUser");
            DropColumn("dbo.Carts", "ModifiedByUser");
            DropColumn("dbo.Carts", "CreatedByUser");
        }
    }
}
