﻿using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.DAL.Repository
{
    public class OrderRepository : RepositoryBase<Order>
    {
        public OrderRepository(PcelariStoreDbContext context) : base(context)
        {

        }
    }
}
