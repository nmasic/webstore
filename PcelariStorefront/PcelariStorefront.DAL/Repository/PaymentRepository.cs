﻿using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.DAL.Repository
{
    public class PaymentRepository : RepositoryBase<Payment>
    {
        public PaymentRepository(PcelariStoreDbContext context) : base(context)
        {

        }
    }
}
