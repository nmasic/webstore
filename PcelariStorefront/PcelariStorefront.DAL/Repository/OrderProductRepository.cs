﻿using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.DAL.Repository
{
    public class OrderProductRepository : RepositoryBase<OrderProduct>
    {
        public OrderProductRepository(PcelariStoreDbContext context) : base(context)
        {

        }
    }
}
