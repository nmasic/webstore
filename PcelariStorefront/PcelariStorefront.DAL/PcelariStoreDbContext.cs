﻿using Microsoft.AspNet.Identity.EntityFramework;
using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.DAL
{
    public class PcelariStoreDbContext : IdentityDbContext<User>
    {
        public PcelariStoreDbContext() : base("PcelariStoreDbContext", throwIfV1Schema: false)
        {

        }

        public static PcelariStoreDbContext Create()
        {
            return new PcelariStoreDbContext();
        }

        public DbSet<ProductType> ProductTypes { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Payment> Payments { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderProduct> OrderProducts { get; set; }

        public DbSet<Cart> CartItems { get; set; }

        //db sets like this that represents database entities
        // public DbSet<Company> Companies { get; set; }
        
    }
}
