﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.Models.DomainModel
{
    public class OrderProduct : EntityBase
    {
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public Order Order { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public Product Product { get; set; }

        [Required]
        public int OrderedQuantity { get; set; }

        public string MerchantId { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        public decimal PurchasePrice { get; set; }

        public OrderProduct(int prodid, decimal price, int qty, string merchant)
        {
            ProductId = prodid;
            PurchasePrice = price;
            OrderedQuantity = qty;
            MerchantId = merchant;
        }

        public OrderProduct()
        {

        }
    }
}
