﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.Models.DomainModel
{
    public class Order : EntityBase
    {
        public string UserId { get; set; }

        public User User { get; set; }

        public string MerchantId { get; set; }

        public User Merchant { get; set; }

        [Required(ErrorMessage = "Total is amount")]
        [Column(TypeName = "Money")]
        public decimal TotalAmount { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int PaymentId { get; set; }

        public Payment Payment { get; set; }
    }
}
