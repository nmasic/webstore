﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.Models.DomainModel
{
    public class Payment : EntityBase 
    {
        [ForeignKey("User")]
        public string UserId { get; set; }

        public User User { get; set; }

        public int OrderId { get; set; }

        public bool BeeKeeperConfirm { get; set; }

        public string UserConfirm { get; set; }

        [Required]
        public DateTime DatePayed { get; set; }
    }
}
