﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.Models.DomainModel
{
    public class Product : EntityBase
    {
        [Required(ErrorMessage = "Product code is mandatory")]
        public string ProductCode { get; set; }

        [ForeignKey("ProductType")]
        public int ProductTypeId { get; set; }

        public ProductType ProductType { get; set; }

        [Required(ErrorMessage = "Name is mandagtory")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Description is mandatory")]
        public string Description { get; set; }

        public int Quantity { get; set; }

        [Required(ErrorMessage = "Price is mandatory")]
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

            


    }
}
