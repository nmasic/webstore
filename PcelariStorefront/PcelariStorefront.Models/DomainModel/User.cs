﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace PcelariStorefront.Models.DomainModel
{
    public class User : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        //public User(string username, string passwordHash, string name, string surname, string oib, bool adminch, string fulladdress,
        //    string secretKey, bool isBeeKeeper, DateTime dateOFRegister) : base(username)
        //{
        //    this.Name = name;
        //    this.Surname = surname;
        //    this.OIB = oib;
        //    this.AdminChecked = adminch;
        //    this.FullAddress = fulladdress;
        //    this.SecretKey = secretKey;
        //    this.IsBeeKeper = isBeeKeeper;
        //    this.DateOfRegister = dateOFRegister;
        //    base.PasswordHash = passwordHash;
        //}

        [Required(ErrorMessage = "Name is mandatory")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Surname is mandatory")]
        [StringLength(100)]
        public string Surname { get; set; }

        [StringLength(11)]
        [RegularExpression(@"^\d+$", ErrorMessage = "OIB is mandatory")]
        [Required]
        public string OIB { get; set; }

        public bool? AdminChecked { get; set; }

        [Required(ErrorMessage = "Address is mandatory")]
        [StringLength(400)]
        public string FullAddress { get; set; }

        [Required(ErrorMessage = "Secret key is mandatory")]
        [StringLength(5, MinimumLength = 5)]
        public string SecretKey { get; set; }
        
        [Required(ErrorMessage = "Please provide information are you a BeeKeeper?")]
        public bool IsBeeKeper { get; set; }

        public DateTime DateOfRegister { get; set; }


    }
}
