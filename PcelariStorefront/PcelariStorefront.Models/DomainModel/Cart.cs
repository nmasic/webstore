﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PcelariStorefront.Models.DomainModel
{
    public class Cart : EntityBase
    {
        public string UserId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }

        public Cart(string userId, int prodId, int qty)
        {
            this.UserId = userId;
            this.ProductId = prodId;
            this.Quantity = qty;
        }

        public Cart()
        {

        }
    }
}
