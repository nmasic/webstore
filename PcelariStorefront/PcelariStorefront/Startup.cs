﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PcelariStorefront.Startup))]
namespace PcelariStorefront
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
