﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PcelariStorefront.Models
{
    public class CartViewModel
    {
        public string Sifra { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

        public CartViewModel(string s,string n, int q, decimal d, int i)
        {
            Sifra = s; Name = n; Quantity = q; Price = d; Id = i;
        }
    }
}