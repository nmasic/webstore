﻿using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PcelariStorefront.Models
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        public decimal total { get; set; }
        public DateTime delivery { get; set; }
        public List<OrderProduct> items { get; set; }

        public OrderViewModel(int id, decimal t, DateTime d, List<OrderProduct> op)
        {
            OrderId = id; total = t; delivery = d; items = op;
        }
    }
}