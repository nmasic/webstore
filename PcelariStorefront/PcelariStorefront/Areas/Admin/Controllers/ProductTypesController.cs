﻿using Ninject;
using PcelariStorefront.DAL.Repository;
using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PcelariStorefront.Areas.Admin.Controllers
{
    public class ProductTypesController : BaseAdminController
    {
        [Inject]
        public ProductTypeRepository productTypeRepository { get; set; }
        // GET: Admin/ProductTypes
        public ActionResult ProductTypes()
        {
            List<ProductType> model = new List<ProductType>();
            model = DbContext.ProductTypes.ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProductType model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedByUser = UserId;
                this.productTypeRepository.Add(model, autoSave: true);

                return RedirectToAction("ProductTypes");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ProductType pt = new ProductType();
            pt = productTypeRepository.Find(id);
            return View(pt);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditProcutType(int id)
        {
            var model = this.productTypeRepository.Find(id);

            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                model.ModifiedByUser = UserId;
                this.productTypeRepository.Update(model, autoSave: true);
                return RedirectToAction("ProductTypes");
            }
            return View(model);
        }

        
    }
}