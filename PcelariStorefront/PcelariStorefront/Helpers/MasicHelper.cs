﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Pcelaristorefront.Helpers
{
    public static class MasicHelper
    {
        public static IDisposable BeginAuthLi(this HtmlHelper helper, string userRole)
        {
            if (HttpContext.Current.User.IsInRole(userRole)) { 
                helper.ViewContext.Writer.Write(string.Format("<li style=\"display: inline;\">"));
            }else
            {
                helper.ViewContext.Writer.Write(string.Format("<li style=\"display: none;\">"));
            }

            return new MyViewLi(helper);
        }

        public static IDisposable BeginAuthDiv(this HtmlHelper helper, string userRole)
        {
            if (HttpContext.Current.User.IsInRole(userRole))
            {
                helper.ViewContext.Writer.Write(string.Format("<div style=\"display: inline;\">"));
            }
            else
            {
                helper.ViewContext.Writer.Write(string.Format("<div style=\"display: none;\">"));
            }

            return new MyViewDiv(helper);
        }

        class MyViewLi : IDisposable
        {
            private HtmlHelper helper;

            public MyViewLi(HtmlHelper helper)
            {
                this.helper = helper;
            }

            public void Dispose()
            {
                this.helper.ViewContext.Writer.Write("</li>");
            }
        }


        class MyViewDiv : IDisposable
        {
            private HtmlHelper helper;

            public MyViewDiv(HtmlHelper helper)
            {
                this.helper = helper;
            }

            public void Dispose()
            {
                this.helper.ViewContext.Writer.Write("</div>");
            }
        }

    }
}