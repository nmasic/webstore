﻿using Ninject;
using PcelariStorefront.DAL.Repository;
using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PcelariStorefront.Controllers
{
    public class ProductController : BaseController
    {
        [Inject]
        public ProductRepository ProductRepository { get; set; }

        // GET: Product
        public ActionResult Index()
        {
            List<Product> modelForView = DbContext.Products
                                            .Include(pt => pt.ProductType)
                                            .Where(prod => prod.UserId == UserId)
                                            .ToList();
            if (modelForView != null)
                return View(modelForView);
            else return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            this.GetPossibleTypes();
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            this.GetPossibleTypes();
            Product model = ProductRepository.Find(id);
            return View(model);
        }

        [HttpGet]
        [Route(Name = "Obrisi_Proizvode")]
        public ActionResult Delete(int id)
        {
            this.ProductRepository.Delete(id, autoSave: true);
            return View("Index");
        }

        [HttpPost]
        public ActionResult Create(Product model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedByUser = UserId;
                model.UserId = UserId;
                this.ProductRepository.Add(model, autoSave: true);

                return RedirectToAction("Index");
            }
            else
            {
                this.GetPossibleTypes();
                return View(model);
            }
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditProduct(int id)
        {
            var model = this.ProductRepository.Find(id);

            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                model.ModifiedByUser = UserId;
                this.ProductRepository.Update(model, autoSave: true);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        private void GetPossibleTypes()
        {
            var possibleTypes = DbContext.ProductTypes.ToList();
            var selectItems = new List<System.Web.Mvc.SelectListItem>();

            //Polje je opcionalno
            var listItem = new SelectListItem();
            listItem.Text = "- odaberite -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var type in possibleTypes)
            {
                listItem = new SelectListItem();
                listItem.Text = type.Name;
                listItem.Value = type.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }

            ViewBag.PossibleTypes = selectItems;
        }
    }
}