﻿using Ninject;
using PcelariStorefront.DAL.Repository;
using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PcelariStorefront.Controllers
{
    public class MOrderController : BaseController
    {
        [Inject]
        public OrderRepository OrderRepository { get; set; }

        // GET: MOrder
        public ActionResult Index()
        {
            List<OrderProduct> myOrderProducts = DbContext.OrderProducts.Where(op => op.MerchantId == UserId).ToList();
            List<Order> myOrders = new List<Order>();

            foreach(OrderProduct op in myOrderProducts)
            {
                Order o = OrderRepository.Find(op.OrderId);
                myOrders.Add(o);
            }

            List<Order> model = myOrders.Distinct().OrderBy(o => o.DateCreated).ToList();

            return View(model);
        }
    }
}