﻿using PcelariStorefront.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PcelariStorefront.Controllers
{
    [RequireHttps]
    public class BaseController : Controller
    {
        private string userId { get; set; }

        public string UserId
        {
            get
            {
                return this.HttpContext.GetOwinContext().Authentication.User.Identity.Name;
            }
        }

        public PcelariStoreDbContext DbContext
        {
            get
            {
                return new PcelariStoreDbContext();
            }
        }
    }
}