﻿using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PcelariStorefront.Controllers
{
    public class UOrderController : BaseController
    {
        // GET: MOrder
        public ActionResult Index()
        {
            List<Order> myOrders = DbContext.Orders.Where(o => o.UserId == UserId).ToList();
            return View(myOrders);
        }
    }
}