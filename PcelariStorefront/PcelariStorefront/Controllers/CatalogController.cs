﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PcelariStorefront.Models.DomainModel;
using PcelariStorefront.DAL.Repository;
using System.Data.Entity;

namespace PcelariStorefront.Controllers
{
    public class CatalogController : BaseController
    {
        [Inject]
        public ProductRepository ProductRepository { get; set; }

        // GET: Catalog
        public ActionResult Index()
        {
            List<User> Merchants = DbContext.Users.Where(u => u.IsBeeKeper).ToList();
            return View(Merchants);
        }

        [HttpGet]
        public ActionResult Products(string uid)
        {
            User usr = DbContext.Users.Where(u => u.Id.Equals(uid)).First();
            List<Product> products = DbContext.Products.Include(pt => pt.ProductType).Where(p => p.UserId == usr.UserName && p.Quantity > 0).ToList();
            return View(products);
        }
    }
}