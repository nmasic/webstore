﻿using Ninject;
using PcelariStorefront.DAL.Repository;
using PcelariStorefront.Models;
using PcelariStorefront.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace PcelariStorefront.Controllers
{
    public class CartController : BaseController
    {
        [Inject]
        public CartRepository CartRepository { get; set; }
        [Inject]
        public ProductRepository ProductRepository { get; set; }
        [Inject]
        public OrderRepository OrderRepository { get; set; }
        [Inject]
        public OrderProductRepository OrderProductRepository { get; set; }

        public ActionResult Index()
        {
            List<Cart> myCartEntries = DbContext.CartItems.Where(c => c.UserId == UserId).ToList();
            List<CartViewModel> modelList = new List<CartViewModel>();
            foreach(Cart c in myCartEntries)
            {
                Product p = ProductRepository.Find(c.ProductId);
                modelList.Add(new CartViewModel(p.ProductCode, p.Name, c.Quantity, p.Price, p.ID));
            }
            return View(modelList);
        }

        // GET: Cart
        [Route(Name = "AddToCart")]
        public ActionResult Add(int id)
        {
            Cart c = new Cart(UserId, id, 1);
            c.CreatedByUser = UserId;
            CartRepository.Add(c, true);
            Product p = ProductRepository.Find(id);
            p.Quantity = p.Quantity - 1;
            ProductRepository.Update(p, true);
            return RedirectToAction("Index", "Catalog");
        }

        [HttpPost]
        public JsonResult Update(int id, int qty)
        {
            Cart c = DbContext.CartItems.Where(cr => cr.ProductId == id).FirstOrDefault();
            if(c != null)
            {
                
                Product p = ProductRepository.Find(c.ProductId);
                if(qty > p.Quantity)
                {
                    return null;
                } else {
                    c.Quantity = qty;
                    p.Quantity = p.Quantity - qty;
                    CartRepository.Update(c, true);
                    ProductRepository.Update(p, true);
                }
            }
            return new JsonResult() { Data = "OK", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult PlaceOrder()
        {
            List<Cart> itemsFromCart = DbContext.CartItems.Where(c => c.UserId == UserId).ToList();
            List<OrderProduct> productsOnOrder = new List<OrderProduct>();

            foreach(Cart c in itemsFromCart)
            {
                Product p = ProductRepository.Find(c.ProductId);
                productsOnOrder.Add(new OrderProduct(p.ID, p.Price, c.Quantity, p.UserId));
            }

            decimal total = 0;

            foreach(OrderProduct op in productsOnOrder)
            {
                total += (op.OrderedQuantity * op.PurchasePrice);
            }

            Order o = new Order();
            o.UserId = UserId;
            o.DeliveryDate = DateTime.Now.AddDays(3);
            o.TotalAmount = total;

            OrderRepository.Add(o, true);

            foreach(OrderProduct op in productsOnOrder)
            {
                op.OrderId = o.ID;
                OrderProductRepository.Add(op, true);
            }

            foreach (var item in itemsFromCart)
            {
                CartRepository.Delete(item.ID, true);
            }

            List<OrderProduct> opList = DbContext.OrderProducts.Include(p => p.Product).Where(oo => oo.OrderId == o.ID).ToList();

            OrderViewModel ovm = new OrderViewModel(o.ID, o.TotalAmount, o.DeliveryDate, opList);

            return View(ovm);
        }
    }
}