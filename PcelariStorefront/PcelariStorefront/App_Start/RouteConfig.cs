﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PcelariStorefront
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Obrisi_Proizvode",
                url: "proizvodi/obrisi/{id}",
                defaults: new { controller = "Product", action = "Delete" },
                constraints: new { id = "[0-9]+" }
            );

            routes.MapRoute(
                name: "AddToCart",
                url: "kosarica/dodaj/{id}",
                defaults: new { controller = "Cart", action = "Add" },
                constraints: new { id = "[0-9]+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
